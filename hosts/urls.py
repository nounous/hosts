from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include

import hosts_views.views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/login/', auth_views.LoginView.as_view()),
    path('accounts/logout/', auth_views.LogoutView.as_view()),
    path('accounts/profile/', hosts_views.views.profile),
    path('api/hosts/', hosts_views.views.api_hosts),
    path('clubs/', hosts_views.views.clubs),
    path('hosts/', hosts_views.views.hosts),
    path('password/', hosts_views.views.password),
    path('ports/', hosts_views.views.ports),
    path('ports/opening/', hosts_views.views.ports_opening),
]

import crypt
import hmac
import json
import re

from django.contrib.auth.models import User

import ldap

from .settings import LDAP_URL, USERNAME_REGEX, USERS_JSON


LDAP_CONNECTIONS = {}

class LdapBackend:

    def authenticate(self, request, username=None, password=None):
        base = ldap.initialize(LDAP_URL)
        if LDAP_URL.startswith('ldaps://'):
            base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
            base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
        if re.fullmatch(USERNAME_REGEX, username) is None:
            return None
        try:
            base.simple_bind_s(f'cn={username},ou=users,dc=adh,dc=crans,dc=org', password)
        except:
            return None
        try:
            user = User.objects.get(username=username)
            LDAP_CONNECTIONS[username] = base
        except User.DoesNotExist:
            user = User(username=username)
            user.save()
            LDAP_CONNECTIONS[username] = base
        return user

    def get_user(self, user_id):
        try:
            user = User.objects.get(pk=user_id)
            try:
                user.base = LDAP_CONNECTIONS[user.username]
            except KeyError:
                return None
            return user
        except User.DoesNotExist:
            return None

class JsonBackend:

    def authenticate(self, request, username=None, password=None):
        json_users = []
        for file in USERS_JSON:
            with open(file) as file:
                json_users.extend(json.load(file))
        for json_user in json_users:
            if json_user['username'] == username:
                if hmac.compare_digest(crypt.crypt(password, json_user['password']), json_user['password']):
                    try:
                        user = User.objects.get(username=username)
                        user.is_staff = json_user['is_staff']
                        user.is_superuser = json_user['is_superuser']
                        user.save()
                    except User.DoesNotExist:
                        user = User(username=username)
                        user.is_staff = json_user['is_staff']
                        user.is_superuser = json_user['is_superuser']
                        user.save()
                    return user
                else:
                    return None
        return None

    def get_user(self, user_id):
        try:
            user = User.objects.get(pk=user_id)
            return user
        except User.DoesNotExist:
            return None

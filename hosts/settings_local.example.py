import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'changeme'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS_LOCAL = []

LDAP_URL = 'ldapi://'
USERNAME_REGEX = '[a-z][a-z0-9-]*'

USERS_JSON = [
    os.path.join(BASE_DIR, 'hosts', 'users.json'),
]

from django.apps import AppConfig


class HostsViewsConfig(AppConfig):
    name = 'hosts_views'

import crypt
import re

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template import loader

import ldap
import ldap.modlist

from .forms import PasswordChangeForm, PortsForm


@login_required
def clubs(request):
    base = request.user.base
    query = base.search_s('ou=clubs,dc=adh,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'objectClass=organization')
    clubs = []
    for dn, entry in query:
        clubs.append(entry['o'][0].decode('utf-8'))
    return render(request, 'clubs.html', {'clubs': clubs})

@login_required
def hosts(request):
    base = request.user.base
    query = base.search_s('ou=hosts,dc=adh,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'objectClass=ipHost')
    hosts = []
    for dn, entry in query:
        name = entry['cn'][0].decode('utf-8')
        ipv4 = [ip.decode('utf-8') for ip in entry['ipHostNumber'] if b'.' in ip]
        ipv6 = [ip.decode('utf-8') for ip in entry['ipHostNumber'] if b':' in ip]
        mac = entry['macAddress'][0].decode('utf-8')
        owner = entry['owner'][0].decode('utf-8')
        if owner.endswith(',ou=users,dc=adh,dc=crans,dc=org'):
            owner = owner[3:-len(',ou=users,dc=adh,dc=crans,dc=org')]
        elif owner.endswith(',ou=clubs,dc=adh,dc=crans,dc=org'):
            owner = owner[2:-len(',ou=clubs,dc=adh,dc=crans,dc=org')] + " (club)"
        vmid = entry['serialNumber'][0].decode('utf-8') if 'serialNumber' in entry else 'aucun :D'
        hosts.append((name, owner, ipv4[0], ipv6[0], mac, vmid))
    hosts.sort(key=lambda host: host[0])
    return render(request, 'hosts.html', {'hosts': hosts})

@login_required
def profile(request):
    base = request.user.base
    username = request.user.username
    entry = base.search_s(f'cn={username},ou=users,dc=adh,dc=crans,dc=org', ldap.SCOPE_BASE, 'objectClass=inetOrgPerson')[0][1]
    user = {}
    for attribute in entry:
        if attribute == 'description':
            for description in entry['description']:
                description = description.decode('utf-8').split(':', 1)
                user[description[0]] = description[1]
        elif attribute not in {'objectClass'}:
            user[attribute] = entry[attribute][0].decode('utf-8')
    return render(request, 'profile.html', {'user': user})

@login_required
def ports(request):
    base = request.user.base
    query = base.search_s('ou=hosts,dc=adh,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'description=*')
    ports_openings = []
    for dn, entry in query:
        host = entry['cn'][0].decode('utf-8')
        ports_opening = entry['description'][0].decode('utf-8').split(',')
        for i in range(len(ports_opening)):
            match = re.fullmatch(r'(?P<protocol>tcp|udp|ipv6)\.(?P<direction>in|out)(?::(?P<start>[0-9]+)(?:-(?P<end>[0-9]+))?)?', ports_opening[i])
            if match is None:
                continue
            protocol = match.group('protocol')
            direction = match.group('direction')
            start = int(match.group('start')) if match.group('start') is not None else None
            end = int(match.group('end')) if match.group('end') is not None else start
            ports_openings.append((host, protocol, direction, start, end))
    ports_openings.sort()
    return render(request, 'ports.html', {'ports_openings': ports_openings})

@login_required
def ports_opening(request):
    base = request.user.base
    if request.method == 'POST':
        form = PortsForm(request.POST, base=base)
        if form.is_valid():
            host = form.cleaned_data['host']
            protocol = form.cleaned_data['protocol']
            direction = 'out' if form.cleaned_data['direction'] else 'in'
            start = form.cleaned_data['start']
            end = form.cleaned_data['end']
            return HttpResponse('')
    else:
        form = PortsForm(base=base)
    return render(request, 'ports_opening.html', {'form': form})

@login_required
def password(request):
    base = request.user.base
    if request.method == 'POST':
        form = PasswordChangeForm(request.POST, base=base)
        if form.is_valid():
            password = form.cleaned_data['password']
            password = f'{{CRYPT}}{crypt.crypt(password, salt=crypt.mksalt(method=crypt.METHOD_SHA512))}'
            dn, entry = base.search_s('ou=users,dc=adh,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'userPassword=*')[0]
            current_password = entry['userPassword'][0]
            ldif = ldap.modlist.modifyModlist(
                {'userPassword': [current_password]},
                {'userPassword': [password.encode('utf-8')]},
            )
            base.modify_s(dn, ldif)
            return HttpResponse("Succès.")
        else:
            return HttpResponse("Échec.")
    else:
        form = PasswordChangeForm(base=base)
    return render(request, 'password.html', {'form': form})

@login_required
def api_hosts(request):
    base = request.user.base
    query = base.search_s('ou=hosts,dc=adh,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'objectClass=ipHost')
    hosts = {}
    for dn, entry in query:
        owner = entry['owner'][0].decode('utf-8')
        owner_type = 'user' if owner.endswith(',ou=users,dc=adh,dc=crans,dc=org') else 'club' if owner.endswith(',ou=clubs,dc=adh,dc=crans,dc=org') else None
        if owner_type == 'user':
            owner = owner[3:-len(',ou=users,dc=adh,dc=crans,dc=org')]
        elif owner_type == 'club':
            owner = owner[2:-len(',ou=users,dc=adh,dc=crans,dc=org')]
        else:
            owner = None
        hosts[entry['cn'][0].decode('utf-8')] = {
            'ip': [ip.decode('utf-8') for ip in entry['ipHostNumber']],
            'mac': entry['macAddress'][0].decode('utf-8'),
            'owner_type': owner_type,
            'owner': owner,
        }
    return JsonResponse(hosts)

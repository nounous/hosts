import crypt
import hmac

from django import forms
from django.core.exceptions import ValidationError

import ldap


class PasswordChangeForm(forms.Form):

    old_password = forms.CharField(
        label="Ancien mot de passe",
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
        strip=False,
    )
    password = forms.CharField(
        label="Nouveau mot de passe",
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        strip=False,
    )
    password_verify = forms.CharField(
        label="Nouveau mot de passe (confirmation)",
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        strip=False,
    )

    def __init__(self, *args, **kwargs):
        self.base = kwargs.pop('base')
        super().__init__(*args, **kwargs)

    def clean_old_password(self):
        old_password = self.cleaned_data['old_password']
        current_password = self.base.search_s('ou=users,dc=adh,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'userPassword=*')[0][1]['userPassword'][0].decode('utf-8').lstrip('{CRYPT}')
        if not hmac.compare_digest(crypt.crypt(old_password, current_password), current_password):
            raise ValidationError("Mot de passe incorrect.")

    def clean_password_verify(self):
        password = self.cleaned_data['password']
        password_verify = self.cleaned_data['password_verify']
        if password and password_verify:
            if password != password_verify:
                raise ValidationError("Impossible de vérifier le mot de passe.")

def hosts(base):
    hosts = [entry['cn'][0].decode('utf-8') for dn, entry in base.search_s('ou=hosts,dc=adh,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'objectClass=ipHost')]
    hosts.sort()
    return zip(hosts, hosts)

class PortsForm(forms.Form):

    host = forms.ChoiceField(choices=())
    protocol = forms.ChoiceField(choices=(('tcp', "TCP"), ('udp', "UDP"), ('ipv6', "6in4")))
    direction = forms.ChoiceField(choices=((0, "in"), (1, "out")))
    start = forms.IntegerField(min_value=0, max_value=65535, required=False)
    end = forms.IntegerField(min_value=0, max_value=65535, required=False)

    def __init__(self, *args, **kwargs):
        base = kwargs.pop('base')
        super().__init__(*args, **kwargs)
        self.fields['host'].choices = hosts(base)
